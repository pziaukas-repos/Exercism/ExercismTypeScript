const Codons = {
    Methionine: ['AUG'],
    Phenylalanine: ['UUU', 'UUC'],
    Leucine: ['UUA', 'UUG'],
    Serine: ['UCU', 'UCC', 'UCA', 'UCG'],
    Tyrosine: ['UAU', 'UAC'],
    Cysteine: ['UGU', 'UGC'],
    Tryptophan: ['UGG'],
};
const StopCodons = ['UAA', 'UAG', 'UGA'];

class ProteinTranslation {
    static proteins(strand: string): Array<string> {
        const result = [];
        for (let i = 0; i < strand.length; i += 3) {
            const codon = strand.slice(i, i + 3);
            if (StopCodons.includes(codon)) {
                break;
            }
            const protein = Object.entries(Codons).find(([, codons]) => codons.includes(codon))![0]
            result.push(protein)
        }
        return result;
    }
}

export default ProteinTranslation

export class Words {
    getWords(sentence: string): Array<string> {
        return sentence
            .trim()
            .toLowerCase()
            .split(/\s+/)
    }

    count(sentence: string): Map<string, number> {
        const countByWord = new Map<string, number>()
        for (const word of this.getWords(sentence)) {
            const count = countByWord.get(word) ?? 0
            countByWord.set(word, count + 1)
        }
        return countByWord
    }
}

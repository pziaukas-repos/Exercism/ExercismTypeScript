export class Squares {
    count: number;

    constructor(count: number) {
        this.count = count;
    }

    public get squareOfSum(): number {
        return (this.count * (this.count + 1) / 2) ** 2;
    }

    public get sumOfSquares(): number {
        return this.count * (this.count + 1) * (2 * this.count + 1) / 6;
    }

    public get difference(): number {
        return this.squareOfSum - this.sumOfSquares
    }
}

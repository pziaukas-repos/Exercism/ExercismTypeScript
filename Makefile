.PHONY: build lint test
$(VERBOSE).SILENT:

## Build the packages
build:
	./run.sh yarn install

## Inspect the code style using eslint
lint:
	./run.sh yarn lint

## Test the exercises
test:
	./run.sh yarn test

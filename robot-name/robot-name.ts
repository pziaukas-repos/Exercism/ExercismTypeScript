function getRandomNumber(limit: number): number {
  return Math.floor(Math.random() * limit);
}

function getRandomLetter(): string {
  return String.fromCharCode(65 + getRandomNumber(26));
}


export default class Robot {
  private static _knownNames = new Set<string>();
  private _name = "";

  constructor() {
    this.resetName();
  }

  private static generateName(): string {
    let name = "";
    
    for (let letter = 0; letter < 2; letter++) {
      name += getRandomLetter();
    }

    for (let number = 0; number < 3; number++) {
      name += getRandomNumber(10);
    }

    return name;
  }

  public get name(): string {
    return this._name;
  }

  public resetName(): void {
    while (Robot._knownNames.has(this._name = Robot.generateName())) {
      // Do nothing
    }
    Robot._knownNames.add(this._name);
  }

  public static releaseNames(): void {
    this._knownNames = new Set();
  }
}

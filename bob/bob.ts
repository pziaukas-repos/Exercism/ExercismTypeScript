class Bob {
    hey(phrase: string): string {
        if (Bob.isEmpty(phrase)) {
            return 'Fine. Be that way!';
        }
        if (Bob.isQuestion(phrase)) {
            if (Bob.isShouting(phrase)) {
                return 'Calm down, I know what I\'m doing!';
            }
            return 'Sure.'
        }
        if (Bob.isShouting(phrase)) {
            return 'Whoa, chill out!';
        }
        return 'Whatever.'
    }

    private static isEmpty(phrase: string): boolean {
        return !phrase.trim();
    }

    private static isShouting(phrase: string): boolean {
        return phrase === phrase.toUpperCase() && phrase !== phrase.toLowerCase();
    }

    private static isQuestion(phrase: string): boolean {
        return phrase.trim().endsWith('?');
    }
}

export default Bob;

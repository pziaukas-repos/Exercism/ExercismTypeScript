#!/usr/bin/env bash

main() {
  for dir in */
  do
    cd ${dir}
    echo "Working in '${dir%*/}'..."
    eval "$@"
    if [[ $? -ne 0 ]]; then
      exit 1
    fi
    cd ..
  done
}

main "$@"

type LettersByScore = Record<number, Array<string>>;
type ScoreByLetter = Record<string, number>;

function transform(oldRules: LettersByScore): ScoreByLetter {
    const newRules: ScoreByLetter = {};

    for (const [scoreString, letters] of Object.entries(oldRules)) {
        for (const letter of letters) {
            newRules[letter.toLowerCase()] = Number(scoreString);
        }
    }

    return newRules;
}

export default transform

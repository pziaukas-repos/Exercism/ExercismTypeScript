function isLeapYear(year: number): boolean {
    const isDivisibleBy = (divisor: number): boolean => year % divisor == 0;
    return isDivisibleBy(4) && (!isDivisibleBy(100) || isDivisibleBy(400));
}

export default isLeapYear

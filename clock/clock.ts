function mod(x: number, m: number): number {
    return ((x % m) + m) % m;
}

class Clock {
    hour: number;
    minute: number;

    constructor(hour: number, minute: number = 0) {
        const hour_extra = Math.floor(minute / 60)
        this.minute = mod(minute, 60);
        this.hour = mod(hour + hour_extra, 24);
    }

    public toString(): string {
        function digit(x: number): string {
            return String(x).padStart(2, "0");
        }

        return `${ digit(this.hour) }:${ digit(this.minute) }`
    }

    public plus(minute: number): Clock {
        return new Clock(this.hour, this.minute + minute);
    }

    public minus(minute: number): Clock {
        return this.plus(-minute);
    }

    public equals(that: Clock): boolean {
        return this.toString() === that.toString();
    }
}

export default Clock

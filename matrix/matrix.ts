class Matrix {
    public rows: number[][];
    public columns: number[][];

    constructor(matrixString: string) {
        this.rows = matrixString
            .split("\n")
            .map(row => row
                .split(" ")
                .map(value => Number(value))
            );

        this.columns = this.rows[0]
            .map((_, index) => this.rows
                .map(row => row[index])
            );
    }
}

export default Matrix

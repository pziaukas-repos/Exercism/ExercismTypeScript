export default class Triangle {
    private readonly sidesSorted: number[];

    constructor(...sides: number[]) {
        this.sidesSorted = sides.sort((x, y) => x - y);
    }

    kind(): string {
        const [a, b, c] = this.sidesSorted;
        let triangleType: string;

        if (a + b <= c) {
            throw new Error('Triangle is not valid');
        } else if (a == c) {
            triangleType = 'equilateral';
        } else if (a == b || b == c) {
            triangleType = 'isosceles';
        } else {
            triangleType = 'scalene';
        }

        return triangleType;
    }
}

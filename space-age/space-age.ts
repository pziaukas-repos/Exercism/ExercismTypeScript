const OrbitalPeriods = {
    Mercury: 0.2408467,
    Venus: 0.61519726,
    Earth: 1,
    Mars: 1.8808158,
    Jupiter: 11.862615,
    Saturn: 29.447498,
    Uranus: 84.016846,
    Neptune: 164.79132,
}

type Planet = keyof typeof OrbitalPeriods;

class SpaceAge {
    private readonly secondsInYear: number = 365.25 * 24 * 60 * 60;
    public readonly seconds: number;

    constructor(ageInSeconds: number) {
        this.seconds = ageInSeconds;
    }

    private calculateAge(planet: Planet): number {
        const age = this.seconds / this.secondsInYear / OrbitalPeriods[planet];
        return Number(age.toFixed(2));
    }

    public onMercury = (): number => this.calculateAge('Mercury');
    public onVenus = (): number => this.calculateAge('Venus');
    public onEarth = (): number => this.calculateAge('Earth');
    public onMars = (): number => this.calculateAge('Mars');
    public onJupiter = (): number => this.calculateAge('Jupiter');
    public onSaturn = (): number => this.calculateAge('Saturn');
    public onUranus = (): number => this.calculateAge('Uranus');
    public onNeptune = (): number => this.calculateAge('Neptune');

}

export default SpaceAge

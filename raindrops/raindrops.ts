class Raindrops {
    private readonly sounds: Record<number, string> = {
        3: 'Pling',
        5: 'Plang',
        7: 'Plong',
    }

    public convert(value: number): string {
        let valueSound = '';
        for (const n in this.sounds) {
            if (value % Number(n) === 0) {
                valueSound += this.sounds[n]
            }
        }
        return (valueSound.length > 0 ? valueSound : value.toString());
    }
}

export default Raindrops

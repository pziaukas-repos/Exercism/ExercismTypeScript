class GradeSchool {
    journal: Map<string, number> = new Map();

    public addStudent(name: string, grade: number): void {
        this.journal.set(name, grade);
    }

    public studentsInGrade(grade: number): Array<string> {
        return [...this.journal.keys()]
            .filter(student => this.journal.get(student) === grade)
            .sort();
    }

    public studentRoster(): Map<string, Array<string>> {
        const roster = new Map();
        const grades = Array
            .from(new Set(this.journal.values()))
            .sort()
        for (const grade of grades) {
            roster.set(String(grade), this.studentsInGrade(grade));
        }
        return roster;
    }
}

export default GradeSchool

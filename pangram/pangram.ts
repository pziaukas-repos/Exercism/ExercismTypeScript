class Pangram {
    private readonly alphabetSize = 26;
    private readonly nonLowercaseMatch = /[^a-z]/g;

    constructor(private sentence: string) {}

    public isPangram(): boolean {
        const differentLetters = new Set(this.sentence.toLowerCase().replace(this.nonLowercaseMatch, ''));
        return differentLetters.size == this.alphabetSize;
    }
}

export default Pangram

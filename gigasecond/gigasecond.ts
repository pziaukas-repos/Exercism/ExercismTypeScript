class Gigasecond {
    private readonly moment: Date;
    private readonly millisecondsToAdd: number = 10 ** 12;

    constructor(moment: Date) {
        this.moment = moment;
    }

    public date(): Date {
        return new Date(this.moment.getTime() + this.millisecondsToAdd);
    }

}

export default Gigasecond

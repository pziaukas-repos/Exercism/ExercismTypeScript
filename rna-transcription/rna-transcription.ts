type DNA = 'A' | 'C' | 'G' | 'T';
type RNA = 'A' | 'C' | 'G' | 'U';

class Transcriptor {
    private complements: Record<DNA, RNA> = {
        'G': 'C',
        'C': 'G',
        'T': 'A',
        'A': 'U',
    }

    isDnaNucleotide(nucleotide: string): nucleotide is DNA {
        return nucleotide in this.complements;
    }

    public toRna(dnaStrand: string): string {
        let rnaStrand = '';
        for (const nucleotide of dnaStrand) {
            if (!this.isDnaNucleotide(nucleotide)) {
                throw new Error('Invalid input DNA.')
            }
            rnaStrand += this.complements[nucleotide]
        }
        return rnaStrand
    }
}

export default Transcriptor
